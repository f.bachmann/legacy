SELECT
    anonymous_id
    , max(anonymous_id_clean) as anonymous_id_clean
    , max(sent_at) as sent_at
from (
  SELECT
    e.anonymous_id
    , CASE
        WHEN e.is_amp then
            -- ensure that if the session only has AMP events of the event is the last, we still have an anonymous id
            COALESCE(
                  -- Next not amp anonymous_id
                  LAG(CASE WHEN not e.is_amp THEN e.anonymous_id END, 1) IGNORE NULLS OVER
                  (
                        PARTITION BY e.context_ip, e.context_user_agent
                        ORDER BY e.timestamp DESC
                  )
                  ,
                  -- Prev not amp anonymous_id
                  LAG(CASE WHEN not e.is_amp THEN e.anonymous_id END, 1) IGNORE NULLS OVER
                  (
                        PARTITION BY e.context_ip, e.context_user_agent
                        ORDER BY e.timestamp ASC
                  )

                  , e.anonymous_id
            )
        ELSE e.anonymous_id
     END as anonymous_id_clean
    , e.sent_at
  from {{ ref('__web_events_0') }} e
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
    left join {{ this }} a1 on a1.anonymous_id = e.anonymous_id
  where e.sent_at > (select max(sent_at) from {{ this }} )
    and e.sent_at < (select max(sent_at) from {{this}}) + INTERVAL '100 DAYS'
    and a1.anonymous_id is null
{% else %}
  WHERE e.sent_at > '2018-09-01'
    and e.sent_at < '2018-09-30'
{% endif %}
    and e.anonymous_id is not null
  ) e
group by 1
