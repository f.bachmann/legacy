SELECT
  user_agent
  , sent_at
  , parse_ua_browser(ua.user_agent) as browser_family
  , parse_ua_browser_ver(ua.user_agent) as browser_ver
  , parse_ua_os(ua.user_agent) as os
  , parse_ua_os_ver(ua.user_agent) as os_ver
  , parse_ua_dev(ua.user_agent) as dev
  , parse_ua_dev_brand(ua.user_agent) as dev_brand
from (
    select
        e.context_user_agent::varchar(999) as user_agent
        , max(e.sent_at) as sent_at
    from {{ ref('__web_events_0') }} e
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
        left join {{this}} ua1 on ua1.user_agent = context_user_agent::varchar(999)
    WHERE e.sent_at > (select max(sent_at) from {{this}})
        and e.sent_at < (select max(sent_at) from {{this}}) + INTERVAL '100 DAYS'
        and ua1.user_agent is null

{% else %}
    WHERE e.sent_at < '2018-07-01'
{% endif %}

        and e.context_user_agent is not null
    group by 1
    ) ua
