--
-- SEGMENT SESSION Datamart

CREATE TABLE test.segment_web_sessions
    DISTKEY (11)
    SORTKEY (11, 2,3,4,5)
    as
SELECT
    e.session_id
    --
    , MAX(quandoo_channel) as quandoo_channel
    , MAX(quandoo_source) as quandoo_source
    , MAX(domain) as domain
    --
    , max(unique_user) as unique_user
    , max(e.browser_family) as browser
    , max(e.browser_ver) as browser_version
    , max(e.device) as device
    , max(e.device_brand) as device_brand
    , max(e.is_bot::int)::bool as is_bot
    --
    , min(e.timestamp) as session_time
    , max(e.timestamp) as session_end_time
    , count(*) as events
    , count(CASE WHEN e.event_name in ('x_details_viewed',
                                       'x_listing_viewed',
                                       'x_reservation_started',
                                       'x_reservation_completed') then e.id
                end
        ) as funnel_events
    , min(CASE WHEN e.event_name = 'x_details_viewed' then e.session_event_id end) as first_detail_page
    , min(CASE WHEN e.event_name = 'x_listing_viewed' then e.session_event_id end) as first_listing_page
    , min(CASE WHEN e.event_name = 'x_reservation_started' then e.session_event_id end) as first_reservation_start
    , min(CASE WHEN e.event_name = 'x_reservation_completed' then e.session_event_id end) as first_reservation_complete
    --
    , count(distinct e.reservation_id) as reservations
    , MAX((e.reservation_id is not null)::int)::bool as session_success
    , MAX( e.is_amp::int)::bool as is_amp
    --
    , max(e.merchant_country_iso2) as merchant_country_iso
    , max(e.tc_country) as cookie_country
    , max(e.city_name) as city_name
    , max(e.agent_id) as agent_id
    , max(e.reservation_agent_id) as res_agent_id
    , max(e.portal_channel) as portal_channel
    , max(e.source_group) as source_group
    , max(e.agent_name) as agent_name
    , max(e.agent_group) as agent_group
    , max(e.agent_category) as  agent_category
    , max(e.account_label_group) as account_label_group
    , max(e.account_label) as account_label
    , max(e.origin) as origin
    , max(utm_source) as utm_source
    , max(utm_medium) as utm_medium
    , max(e.context_library_name) as library_name
    , max(e.context_ip) as ip
    , max(e.review_score) as review_score
    , max(e.num_reviews) as num_reviews
    --
    , max(e.dinner_start_time_utc) as dinner_start_time_utc
    , max(r.dinner_start_time_utc) as dinner_start_time_utc_r
    , MIN(CASE WHEN r.status_id = 1 then '1_SUCCESS' ELSE '9_OTHER' END) as status
    -- , LISTAGG(r.status_id) as reservation_status_list
    , count(CASE WHEN r.status_id = 1 then r.reservation_id END) as success_reservation
    , sum(CASE WHEN r.status_id = 1 then r.trx_gm_eur END) as trx_gross_margin
from qdata.web_events e
    left join qdata.reservations r on e.reservation_id = r.reservation_id
where e.timestamp > '2018-07-01'
group by 1

