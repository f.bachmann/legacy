--
--
-- Segment Events x Cookie processing
--
-- TODO: channel and source: wrong
-- TODO: Analyse referrals of other integrations (TimeOut, ...)
WITH start_t as (
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
        SELECT max(t) as t0 from (
            SELECT COALESCE(max(t.timestamp), '2018-01-01') as t from {{ this }} t
            UNION select MIN(e.timestamp) from {{ ref('__web_events_1_users') }} e
        ) t1
{% else %}
        SELECT MIN(e.timestamp) as t0 from {{ ref('__web_events_1_users') }} e
{% endif %}
)

SELECT
    --
    --
    e1.*,
    --
    -- CHANNEL
    --
    TRIM(UPPER(
    CASE
        --
        -- If agent: for main channels use agent (agent_id as extracted from urls/referrers)
        WHEN a.agent_id is NOT NULL THEN
            CASE
                when agent_group='Google' then 'GOOGLE'
                when agent_group='Iovox' then 'CALLS'
                ELSE upper(a.agent_category)
            END

       -- 2 use utm_campaign_name or TC from urls/referrers
       WHEN rc.portal_channel in ('SEM', 'CRM', 'AFFILIATE', 'DISPLAY', 'CALLS', 'SOCIAL') THEN rc.portal_channel

       -- 3 use (other) UTM parameters
       WHEN e1.session_campaign_medium IS NOT NULL THEN
            UPPER(CASE
                    WHEN e1.session_campaign_medium ~ '^coop$|^cooperation[s]?$|^COOPERATION[S]?$|^partnership$' THEN 'cooperations'
                    WHEN e1.session_campaign_medium ~ '^email[_transactional]?$' THEN 'CRM'
                    WHEN e1.session_campaign_medium = 'referral' THEN 'OTHER'
                    WHEN e1.session_campaign_medium ~ '^Call_(inbound)?$' THEN 'CALLS'
                    -- Bing --> SEM
                    WHEN e1.session_campaign_medium = 'cpc' AND e1.session_campaign_source='bing' THEN
                        'SEM'
                    WHEN e1.session_campaign_medium = 'content' THEN 'SOCIAL'
                    WHEN e1.session_campaign_medium = 'test' THEN 'TEST'
                    WHEN e1.session_campaign_source IS NOT NULL THEN
                        CASE
                          WHEN e1.session_campaign_source ~ '^coop$|^cooperation[s]?$|^COOPERATION[S]?$|^partnership$' THEN 'cooperations'
                          WHEN e1.session_campaign_source = 'Adwords' THEN 'SEM'
                          WHEN e1.session_campaign_source ~ '^[F|f]acebook$|^[P|p]interest[k]?$|^[I|i]nstagram$' THEN 'SOCIAL'
                          WHEN e1.session_campaign_source ~ '^[a|A]win$|^[b|B]elboon$|^[t|T]radedoubler$|^[A|a]ffiliate$' THEN 'AFFILIATE'
                          WHEN e1.session_campaign_source = 'Outbrain' THEN 'display'
                          ELSE 'OTHER'
                        END
                    ELSE 'OTHER'
                END)

       WHEN e1.session_campaign_source IS NOT NULL THEN
            UPPER(CASE
                          WHEN e1.session_campaign_source ~ '^coop$|^cooperation[s]?$|^COOPERATION[S]?$|^partnership$' THEN 'cooperations'
                          WHEN e1.session_campaign_source = 'Adwords' THEN 'SEM'
                          WHEN e1.session_campaign_source ~ '^[F|f]acebook$|^[P|p]interest[k]?$|^[I|i]nstagram$' THEN 'SOCIAL'
                          WHEN e1.session_campaign_source ~ '^[a|A]win$|^[b|B]elboon$|^[t|T]radedoubler$' THEN 'AFFILIATE'
                          WHEN e1.session_campaign_source = 'Outbrain' THEN 'display'
                    ELSE 'OTHER'
                  END)

        -- 4 use referrer
        --
        -- SEO <- Referral from search engine
        WHEN e1.session_referrer ~ 'aol|baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|suche.t-online|yahoo|yandex|youdao'
            THEN 'SEO'
        --
        -- DIRECT <- No referal or Quandoo ref
        -- http(&)://*.quandoo.*
        WHEN e1.session_referrer IS NULL
            OR e1.session_referrer ~ '^http[s]?://[a-z0-9-]+\\.quandoo\\.'
            THEN 'DIRECT'
        --
        -- DIRECT <- No referal or Quandoo ref
        -- http(&)://*.quandoo.*
        WHEN e1.session_referrer ~ '^http[s]?://[a-z]+\\.facebook\\.'
            OR e1.session_referrer ~ '^android-app://[a-z]+\\.facebook\\.'
            THEN 'SOCIAL'

        -- INSTAGRAM
        -- This captures merchant with a manually created link to Quandoo (Instagram integration gets channel from agents)
        WHEN e1.session_referrer ~ 'https://l.instagram.com/'
                 OR e1.session_referrer ~ 'http://instagram.com/'
            THEN 'SOCIAL'
        ELSE 'OTHER'
    END)) as quandoo_channel,
    --
    -- SOURCE
    TRIM(UPPER(
    CASE
        --
        -- If agent: for main paid channels use agent (agent_id as extracted from urls/referrers)
        WHEN a.agent_id is NOT NULL THEN
            CASE
                -- WHEN a.agent_id = 63 THEN 'GOOGLE LISTING'
                when agent_group='Google' then upper(agent_name)
                WHEN a.agent_id = 73 THEN upper(agent_name) -- IOVOX
                ELSE upper(a.agent_name)
            END
        -- 2 use utm_campaign_name or TC from urls/referrers
        WHEN rc.portal_channel in ('SEM', 'DISPLAY') THEN
            CASE
                WHEN rc.source='tbd' or rc.source is null THEN 'OTHER'
                ELSE upper(rc.source)
            END
        WHEN rc.portal_channel in ('SOCIAL', 'CALLS') THEN upper(rc.source)
        -- TODO: @Bjoern to fix qdata_static.account_labels
        WHEN rc.portal_channel in ('CRM') or e1.session_campaign_medium = 'email' THEN
            CASE
                    WHEN upper(e1.session_campaign_source) in ('TRANSACTIONAL', 'NEWSLETTER') then e1.session_campaign_source
                    WHEN upper(rc.source) in ('TRANSACTIONAL', 'NEWSLETTER') then rc.source
                    WHEN account_label = 'TRANSX' then 'Transactional'
                    ELSE 'OTHER' --'Newsletter'
             END
        -- 2.1 AFF because there are/were several issues with TimeOut and other AWIN partners that
        --   conflict with TimeOut and GoogleMaps
        WHEN rc.portal_channel = 'AFFILIATE' then
            CASE
                WHEN rc.source_group='tbd' or rc.source_group is null THEN 'OTHER'
                ELSE upper(rc.source_group)
            END

        -- 3 use (other) UTM parameters
        WHEN e1.session_campaign_source IS NOT NULL THEN
            UPPER(CASE
                          WHEN e1.session_campaign_source ~ '^coop$|^cooperation[s]?$|^COOPERATION[S]?$' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_source = 'Adwords' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_source ~ '^[F|f]acebook$|^[P|p]interest[k]?$|^[I|i]nstagram$' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_source ~ '^[a|A]win$|^[b|B]elboon$|^[t|T]radedoubler$' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_source = 'Outbrain' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_source = 'tripadvisor' THEN e1.session_campaign_source
                          WHEN e1.session_campaign_name = 'timeout' THEN 'timeout'
                    ELSE 'OTHER'
                  END)

        -- 4 use referrer. This separates SEO / DIRECT and ORGANIC SOCIAL
        --
        -- SEO <- Referral from search engine
        WHEN e1.session_referrer ~ 'aol|baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|suche.t-online|yahoo|yandex|youdao'
            THEN upper(regexp_substr(session_referrer,'baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|yahoo|yandex|youdao'))
        --
        -- DIRECT <- No referal or Quandoo ref
        -- http(&)://*.quandoo.*
        WHEN e1.session_referrer IS NULL THEN 'DIRECT'
        WHEN e1.session_referrer ~ '^http[s]?://[a-z0-9-]+\\.quandoo\\.'
            THEN 'QUANDOO'
        --
        -- DIRECT <- No referal or Quandoo ref
        -- http(&)://*.quandoo.*
        WHEN e1.session_referrer ~ '^http[s]?://[a-z]+\\.facebook\\.'
            OR e1.session_referrer ~ '^android-app://[a-z]+\\.facebook\\.'
            THEN 'FACEBOOK'

        -- INSTAGRAM
        WHEN e1.session_referrer ~ 'https://l.instagram.com/' OR e1.session_referrer ~ 'http://instagram.com/'
            THEN 'INSTAGRAM'
        ELSE 'OTHER'
    END)) as quandoo_source,
    --
    --
    a.origin,
    a.agent_name,
    a.agent_group,
    a.agent_category,

    -- account_label
    CASE
       --utm_campaign_name / TC only
       WHEN rc.account_label IS NOT NULL THEN rc.account_label

        -- Labels for SEO and Direct
        --
        -- Google Integration (knowledge panel) also goes here
        WHEN e1.session_referrer ~ 'aol|baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|suche.t-online|yahoo|yandex|youdao'
            THEN 'SEO'
        WHEN e1.session_referrer ~ '^http://www.quandoo.|^https://www.quandoo.'
            THEN 'DTI'
        WHEN e1.session_referrer IS NULL
            THEN 'DTI'
        ELSE 'Other'
    END AS account_label,
    -- source -> should be merged/replaced with quandoo_source
    CASE
       --utm_campaign_name / TC only
      WHEN rc.source NOTNULL THEN rc.source
      --
      -- source for SEO and Direct
      WHEN e1.session_referrer ~ 'baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|yahoo|yandex|youdao'
        THEN regexp_substr(session_referrer,'baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|yahoo|yandex|youdao')
      WHEN e1.session_referrer ~ '^http://www.quandoo.|^https://www.quandoo.'
        THEN 'Direct'
      WHEN e1.session_referrer ISNULL
        THEN 'Direct'
      ELSE 'Other'
    END
    AS source,
    -- portal_channel -> should be merged/replaced with quandoo_channel
    CASE
        -- utm_campaign_name / TC only
          WHEN rc.portal_channel NOTNULL THEN rc.portal_channel
        -- portal_channel for SEO and Direct
          WHEN e1.session_referrer ~ 'baidu|bing|duckduckgo|ecosia|exalead|gigablast|google|munax|qwant|sogou|soso|yahoo|yandex|youdao'
            THEN 'SEO'
          WHEN e1.session_referrer ~ '^http://www.quandoo.|^https://www.quandoo.'
            THEN 'DIRECT'
          WHEN e1.session_referrer ISNULL
            THEN 'DIRECT'
          ELSE 'Other'
    END
    AS portal_channel
    --
    -- RC cookie
    , rc.utm_source
    , rc.utm_medium

    , rc.account_label_group
    , rc.medium

    , rc.source_group
    --
    , rc.country
    , rc.language
    --
    , rc.status_campaign as tc_status_campaign
    , rc.crm_country_desc
    , rc.campaign_origin
    , rc.campaign_name
    , rc.campaign_type
    , rc.account_label_id
    , rc.adsource_id
    , rc.campaign_id
    --
    , rc.caller_id
    , rc.afilliate_id
    --
from (
    select
        e.timestamp
        , e.id
        , e.session_id
        , e.user_session_id
        , e.event_type
        , e.event_name
        , e.page_type
        , e.received_at
        , e.sent_at
        , e.uuid
        , e.anonymous_id
        , e.user_id
        , e.customer_id
        , coalesce(e.customer_id::varchar, e.anonymous_id) as unique_user
        , e.email
        , e.context_page_url
        , e.context_ip
        , e.domain
        , e.context_campaign_medium
        , e.context_campaign_source
        , e.context_campaign_name
        , e.context_campaign_content
        , e.context_campaign_term
        , e.tc_tracker
        , e.context_campaign_image_campaign
        , e.context_page_title
        , e.context_page_search
        , e.event_text
        , e.context_library_name
        , e.context_library_version
        , e.context_page_path
        , e.context_page_referrer
        , e.context_user_agent
        --
        -- events in session
        , count(*) over (partition by session_id) as session_event_n
        -- position of event in session
        , row_number() OVER (partition by session_id order by e.timestamp) as session_event_id
        --
        -- SESSION CAMPAIGN

        , FIRST_VALUE(e.tc_tracker IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_tc

        , FIRST_VALUE(e.agent_id IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_agent_id

        , FIRST_VALUE(e.context_campaign_source IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_campaign_source

        , FIRST_VALUE(e.context_campaign_medium IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_campaign_medium

        , FIRST_VALUE(e.context_campaign_name IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_campaign_name
        , FIRST_VALUE(e.context_campaign_content IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_campaign_content
        , FIRST_VALUE(e.context_campaign_term IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_campaign_term
        --
        -- DAY CAMPAIGN
        , FIRST_VALUE(e.tc_tracker IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_tc
        , FIRST_VALUE(e.context_campaign_source IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_campaign_source

        , FIRST_VALUE(e.context_campaign_medium IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_campaign_medium

        , FIRST_VALUE(e.context_campaign_name IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_campaign_name
        , FIRST_VALUE(e.context_campaign_content IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_campaign_content
        , FIRST_VALUE(e.context_campaign_term IGNORE NULLS) OVER (PARTITION BY e.anonymous_id, date(e.timestamp) ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as date_campaign_term
        , FIRST_VALUE(e.gclid IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_gclid
        , FIRST_VALUE(e.context_page_referrer IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_referrer
        , FIRST_VALUE(e.fbclid IGNORE NULLS) OVER (PARTITION BY e.session_id ORDER BY e.timestamp
            ROWS
            BETWEEN UNBOUNDED PRECEDING
            AND UNBOUNDED FOLLOWING) as session_fbclid
    from {{ ref('__web_events_1_users') }} e
    WHERE e.timestamp > (select t0 from start_t)
      and e.timestamp  < (select t0 from start_t) + interval '100 DAYS'

    ) e1
LEFT JOIN {{ var('cookies') }} rc on rc.cookie = COALESCE(session_tc, session_campaign_name)
LEFT JOIN {{ var('agents') }} a on a.agent_id = session_agent_id