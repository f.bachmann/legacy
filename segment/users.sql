--
-- Segment Users history table
--
select
    i.anonymous_id
    , i.user_id
    , coalesce(c.parent_customer_id, c.id) as customer_id
    , c.email
    , coalesce(i.email_address, i.email) as segment_email
    , i.timestamp
    --
    , row_number() over (partition by i.anonymous_id order by i.timestamp) as rn1
    , LAG(i.timestamp) over (partition by i.anonymous_id order by i.timestamp) as prev_t
    --
    , CASE
        WHEN row_number() over (partition by i.anonymous_id order by i.timestamp) = 1 THEN
            '1970-01-01 00:00:00'::timestamp
        ELSE i.timestamp
    end as valid_from
    , LAG(i.timestamp) over (partition by i.anonymous_id order by i.timestamp DESC) as valid_to
from consumer_web.identifies i
    left join booking_public.customers c on c.c_public_id = i.user_id
