--    -- landing page
--    , CASE
--        WHEN FIRST_VALUE(CASE WHEN  e1.event_type ='track' then  e1.event_name end ) over (partition by  e1.session_id ORDER BY  e1.timestamp) in
--             ('x_customer_logged_in', 'x_customer_registered', 'x_details_viewed', 'x_filter_searched', 'x_listing_viewed',
--              'x_reservation_completed', 'x_reservation_started', 'x_text_searched', 'near_me_viewed', 'clicked_email')
--            THEN FIRST_VALUE(CASE WHEN  e1.event_type ='track' then  e1.event_name end ) over (partition by  e1.session_id ORDER BY  e1.timestamp) in
--                 ('x_customer_logged_in', 'x_customer_registered', 'x_details_viewed', 'x_filter_searched', 'x_listing_viewed',
--                  'x_reservation_completed', 'x_reservation_started', 'x_text_searched', 'near_me_viewed', 'clicked_email')
--        ELSE 'OTHER'
--    END as landing_page
--    -- event order
--    , min(case when  e1.event_name = 'x_customer_logged_in' then  trk.session_event_n end) over (partition by  e1.session_id) as first_login
--    , min(case when  e1.event_name = 'x_customer_registered' then  trk.session_event_n end) over (partition by  e1.session_id) as first_registered
--    , min(case when  e1.event_name = 'x_details_viewed' then  trk.session_event_n end) over (partition by  e1.session_id) as first_detail
--    , min(case when  e1.event_name = 'x_filter_searched' then  trk.session_event_n end) over (partition by  e1.session_id) as first_fsearched
--    , min(case when  e1.event_name = 'x_listing_viewed' then  trk.session_event_n end) over (partition by  e1.session_id) as first_listing
--    , min(case when  e1.event_name = 'x_reservation_completed' then  trk.session_event_n end) over (partition by  e1.session_id) as first_rcompleted
--    , min(case when  e1.event_name = 'x_reservation_started' then  trk.session_event_n end) over (partition by  e1.session_id) as first_rstarted
--    , min(case when  e1.event_name = 'x_text_searched' then  trk.session_event_n end) over (partition by  e1.session_id) as first_tsearched
--    , min(case when  e1.event_name = 'near_me_viewed' then  trk.session_event_n end) over (partition by  e1.session_id) as first_nmviewed
--    , min(case when  e1.event_name = 'clicked_email' then  trk.session_event_n end) over (partition by  e1.session_id) as first_email
--    -- sessions with reservation
--    , max(CASE WHEN xe.reservation_id NOTNULL THEN 1 ELSE 0 END) over (partition by  e1.session_id)::BOOLEAN AS converted




with _res as (
    SELECT
        reservation_id
        , rs.status_name AS status
        , r.status_detailed
        , r.agent_id as reservation_agent_id
        , r.source_last as reservation_source
        , r.account_label_last as reservation_account_label
        , r.source_group_last as reservation_source_group
        , r.origin
        , r.trx_gm_eur as  transaction_gross_margin_eur
        , r.trx_gm_lcy as transaction_gross_margin_local
        , r.dinner_start_time_utc
        , r.customer_id
    from qdata.reservations r
        left join qdata_static.reservation_status rs on rs.status_detailed_id = r.status_detailed_id
    where r.booking_time_utc >= '2018-01-01'
)

  , start_t as (
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
        SELECT max(t) as t0 from (
            SELECT COALESCE(max(t.timestamp), '2018-01-01') as t from {{ this }} t
            UNION select MIN(e.timestamp) from {{ ref('__web_events_1_users') }} e
        ) t1
{% else %}
        SELECT MIN(e.timestamp) as t0 from {{ ref('__web_events_1_users') }} e
{% endif %}
  )

select
    e1.timestamp
    , e1.id
    , e1.session_id
    , trk.session_event_id
    , e1.user_session_id
    , e1.event_type
    , e1.event_name
    , e1.page_type
    , e1.received_at
    , e1.sent_at
    --
    -- user
    , e1.uuid
    , e1.anonymous_id
    , e1.user_id
    , e1.customer_id
    , e1.email
    --
    -- Should be kept
    , e1.context_page_url
    , e1.context_ip
    , e1.domain

     , e1.context_campaign_medium
     , e1.context_campaign_source
     , e1.context_campaign_name
     , e1.context_campaign_content
     , e1.context_campaign_term

     -- , e1.agent_id
     -- , e1.tc_tracker
    , e1.context_campaign_image_campaign

    , e1.context_page_title
    , e1.context_page_search
    , e1.event_text

    , e1.context_library_name
    , e1.context_library_version
    , e1.context_page_path
    , e1.context_user_agent
    , e1.context_page_referrer
    --
    , coalesce(e1.customer_id::varchar, e1.anonymous_id) as unique_user
    --
    -- Tracking parameters (calculated on session level)
    , trk.quandoo_channel
    , trk.quandoo_source
    , trk.origin
    , trk.session_agent_id as agent_id
    , trk.agent_name
    , trk.agent_group
    , trk.agent_category
    , trk.account_label_group
    , trk.account_label
    , trk.portal_channel
    , trk.utm_source as utm_source
    , trk.utm_medium as utm_medium
    , trk.medium as tc_medum

    , trk.source_group as source_group
    , trk.country as tc_country
    , trk.language as tc_language

    --
    -- x EVENTS
    , xe.merchant_id
    , xe.merchant_name
    , xe.city_name
    , xe.review_score
    , xe.num_reviews
    , xe.reservation_id
    , xe.visitor_id
    , xe.agent_id as xe_agent_id
    , xe.destination_id
    , xe.merchant_country_iso2
    , xe.is_listed
    , xe.is_bookable
    , xe.merchant_is_new
    , xe.search_query
    , xe.bookable_online
    , xe.smart_offer_filter
    , xe.with_availability
    --
    -- Reservations
    , r.transaction_gross_margin_eur
    , r.transaction_gross_margin_local
    , r.status
    , r.status_detailed
    , r.reservation_account_label
    , r.reservation_source
    , r.reservation_source_group
    , r.origin as reservation_origin
    , r.dinner_start_time_utc
    , r.customer_id as reservation_customer_id
    , r.reservation_agent_id
    --
    , trk.session_gclid as gclid
    , e1.gclid as event_gclid
    , trk.session_event_n
    , e1.context_amp_id
    , e1.original_anonymous_id
    , e1.is_amp AS event_is_amp
    , MAX((e1.is_amp)::INT ) OVER (PARTITION BY e1.session_id )::BOOLEAN as session_is_amp
    , e1.is_bot
    , COALESCE(xe.x_detail_view_is_amp, FALSE) as x_detail_view_is_amp
    , xe.experiment_id_variation
    , ua.browser_family
    , ua.browser_ver
    , ua.dev as device
    , ua.dev_brand as device_brand
    , ua.os
    , ua.os_ver
    , xe.num_results
    , xe.category
    , xe.parent_category
    , xe.country_iso2
    , xe.district
    , trk.session_fbclid as fbclid
    , e1.fbclid as event_fbclid
    , xe.merchants_array
    , trk.session_campaign_name AS utm_campaign
    , trk.session_campaign_term AS utm_term
    , trk.session_campaign_content AS utm_content

from {{ ref('__web_events_1_users') }} e1
    left join {{ ref('__web_events_2_cookies') }} trk on trk.timestamp = e1.timestamp
                                                         and trk.id = e1.id
    left join {{ ref('__web_events_3_xevents') }} xe on xe.timestamp = e1.timestamp
                                                        and xe.id = e1.id
    left join _res r on r.reservation_id = xe.reservation_id
    left join {{ ref('web_user_agents') }} ua on ua.user_agent = e1.context_user_agent
WHERE e1.timestamp > (select t0 from start_t)
  and e1.timestamp  < (select t0 from start_t) + interval '100 DAYS'
