--
-- Segment Web Event 1
--
-- > calculate sessions
-- > extract:
--  >> domain
--  >> TC cookie
--  >> agent_id, ig_ix (Instagram)
--  >> is_amp
--  >> reconcile amp/non-amp anonymous id
-- > join qdata.customers, get customer_id
--
-- > Set TIMESTAMP as Primary Key, SORTKEY and DISTKEY
--
WITH
start_t as (
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
        SELECT max(t) as t0 from (
            SELECT COALESCE(max(t.timestamp), '2018-01-01') as t from {{ this }} t
            UNION select MIN(e.timestamp) from {{ ref('__web_events_0') }} e
        ) t1
{% else %}
        SELECT MIN(e.timestamp) as t0 from {{ ref('__web_events_0') }} e
{% endif %}
),
events_0 as (
    SELECT
        e.id,
        e.timestamp,
        event_type,
        event_name,
        e.page_type,
        e.received_at,
        e.sent_at,
        uuid,
        COALESCE(an.anonymous_id_clean, e.anonymous_id) as anonymous_id,
        --
        e.original_anonymous_id,
        e.user_id,
        e.context_page_url,
        e.context_ip,
        e.context_campaign_medium,
        e.context_campaign_source,
        e.context_campaign_name,
        e.context_campaign_content,
        e.context_campaign_term,
        e.context_campaign_image_campaign,
        e.context_page_title,
        e.context_page_search,
        e.event_text,
        e.context_library_name,
        e.context_library_version,
        e.context_page_path,
        e.context_page_referrer,
        e.context_user_agent,
        e.context_amp_id,
        e.is_amp,
        row_number() OVER (PARTITION BY e.id ORDER BY e.timestamp) AS id_count
    FROM {{ ref('__web_events_0') }} e
    LEFT JOIN {{ ref('__web_anonymous_id') }} an on an.anonymous_id = e.original_anonymous_id
    WHERE e.timestamp > (select t0 from start_t)
      and e.timestamp  < (select t0 from start_t) + interval '100 DAYS'
      and e.id NOT IN (select id from {{this}})
    )
SELECT
    e1.timestamp
    , e1.id
    --
    -- SESSION calculation
    --
    , SUM(e1.new_session)
        OVER (  ORDER BY e1.anonymous_id, e1.sent_at, e1.prev_event_sent_at
                ROWS UNBOUNDED PRECEDING)
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
            + COALESCE( (select max(session_id) from {{this}} ) , 0 )
{% endif %}
          AS session_id
    , SUM(e1.new_session)
        OVER (
          PARTITION BY e1.anonymous_id
          ORDER BY sent_at, e1.prev_event_sent_at
          ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS user_session_id
    --
    , e1.event_type
    , e1.event_name
    , e1.page_type
    , e1.received_at
    , e1.sent_at
    , e1.uuid
    , e1.anonymous_id
    , e1.user_id
    , i.customer_id
    , i.email
--    , e1.customer_id
--    , e1.email
    , e1.context_page_url
    , e1.context_ip
    , e1.domain
    , e1.context_campaign_medium
    , e1.context_campaign_source
    , e1.context_campaign_name
    , e1.context_campaign_content
    , e1.context_campaign_term
    , e1.agent_id -- Agent Id from URL
    , e1.tc_tracker
    , context_campaign_image_campaign
    , context_page_title
    , context_page_search
    , event_text
    , context_library_name
    , context_library_version
    , context_page_path
    , context_page_referrer
    , context_user_agent
    --
    , gclid
    , context_amp_id
    , original_anonymous_id
    , is_amp
    , is_bot
    , fbclid
FROM (
    SELECT
        COALESCE(LAG(sent_at, 1) OVER (PARTITION BY t.anonymous_id ORDER BY sent_at), '1970-01-01T00:00') AS prev_event_sent_at,
        --
        -- If last event before 30 minutes -> NEW SESSION
        CASE
            WHEN datediff('seconds', LAG(sent_at, 1) OVER (PARTITION BY t.anonymous_id ORDER BY sent_at), t.sent_at) < 60*30 then 0
            ELSE 1
        END as new_session
        , t.id as id
        , t.timestamp
        --
        , event_type
        , t.event_name
        , page_type
        --
        , t.received_at
        , t.sent_at
        --
        , t.uuid
        --
        -- For AMP events, pick the anonymous ID of the next non-AMP event.
        , t.anonymous_id
        , t.user_id
        , t.context_page_url
        , t.context_ip
        , CASE WHEN regexp_replace(REGEXP_SUBSTR(t.context_page_url, '^http[s]?://[a-z]+.quandoo\\.(at|co|com|de|it|sg|ch|fi|lu|nl)(\\.com|\\.au|\\.hk|\\.tr|\\.uk)?'),'http[s]?[:]?//','') != '' THEN
    regexp_replace(REGEXP_SUBSTR(t.context_page_url, '^http[s]?://[a-z]+.quandoo\\.(at|co|com|de|it|sg|ch|fi|lu|nl)(\\.com|\\.au|\\.hk|\\.tr|\\.uk)?'),'http[s]?[:]?//','') ELSE 'x - other' END AS domain
        --
        -- Tracking code
        , t.context_campaign_medium
        , t.context_campaign_source
        , t.context_campaign_name
        , t.context_campaign_content
        , t.context_campaign_term
        -- TC
        , CASE
            WHEN t.context_page_search ~ 'aid=[0-9]{1,3}' THEN
                NULLIF(REGEXP_REPLACE(REGEXP_SUBSTR(replace(t.context_page_search,'%', '&'), 'aid=[^&]*'), 'aid=', ''), '')
            WHEN t.context_page_search ~ 'ig_ix=true' THEN
                '92'
        END as agent_id
        , CASE
            WHEN SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_search, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2) <> ''
                THEN SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_search, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2)
            WHEN SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_referrer, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2) <> ''
                THEN SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_referrer, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2)
            WHEN SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_url, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2) <> ''
                THEN  SPLIT_PART(REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_url, '(TC=|TC%3D)([^(&|%)]*)'),'%3D','='), '=', 2)
        END as tc_tracker
        --
        -- OTHER
        , context_campaign_image_campaign
        , context_page_title
        , context_page_search
        , event_text
        , context_library_name
        , context_library_version
        , context_page_path
        , context_page_referrer
        , context_user_agent
        --
        -- Google Click ID
        , REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_url,'gclid=[^&|%]*'),'gclid=','') AS gclid
        , t.context_amp_id
        , t.original_anonymous_id
        , t.is_amp
        --
        -- Detect BOTs
        , t.context_user_agent SIMILAR TO '%(bot|crawl|slurp|spider|archiv|spinn|sniff|seo|audit|survey|pingdom|worm|capture|(browser|screen)shots|analyz|index|thumb|check|facebook|YandexBot|Twitterbot|a_archiver|facebookexternalhit|Bingbot|Googlebot|Baiduspider|360(Spider|User-agent))%' as is_bot
        -- Facebook Click ID
        , REGEXP_REPLACE(REGEXP_SUBSTR(t.context_page_url,'fbclid=[^&|%]*'),'fbclid=','') AS fbclid
    FROM events_0 t
    WHERE t.id_count = 1
) e1
left join {{ ref('users') }} i on i.anonymous_id = e1.anonymous_id
    and e1.timestamp >=  i.valid_from
    and (e1.timestamp < i.valid_to or i.valid_to is null)
