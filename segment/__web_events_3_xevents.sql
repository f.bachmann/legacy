select
    e.timestamp
    , e.id
    --
    -- XDATA
    -- merchant
    -- TODO: join merchant
    , coalesce(x1rc.merchant_id, x2rs.merchant_id, x3dv.merchant_id, xxts.merchant_id) as merchant_id
    , coalesce(x1rc.merchant_name, x2rs.merchant_name, x3dv.merchant_name, xxts.merchant_name_suggested) as merchant_name
    , coalesce(m.city_name, x4lv.city) AS city_name
    , coalesce(x1rc.review_score::varchar, x2rs.review_score::varchar, x3dv.review_score::varchar) as review_score
    , coalesce(x1rc.num_reviews, x2rs.num_reviews, x3dv.num_reviews) as num_reviews
    -- res
    -- TODO: merge reservation
    , x1rc.reservation_id
    , x1rc.agent_id
    , x1rc.visitor_id -- null ?
    -- , r.country_iso2
    , COALESCE(m.destination_id, x4lv.destination_id) AS destination_id
    , m.country_iso2 AS merchant_country_iso2 -- merch
    , m.is_listed
    , m.is_bookable
    , x3dv.merchant_is_new
    -- TS
    , coalesce(x4lv.query, xxts.query, xxfs.query) as search_query --
    , xxfs.bookable_online
    , xxfs.smart_offer_filter
    , x4lv.with_availability
    , coalesce(x1rc.experiment_id_variation, x2rs.experiment_id_variation, x3dv.experiment_id_variation,
               x4lv.experiment_id_variation, xxts.experiment_id_variation, xxfs.experiment_id_variation,
               xxcli.experiment_id_variation, xxcr.experiment_id_variation) as experiment_id_variation
    , COALESCE(x3dv.is_amp, FALSE) as x_detail_view_is_amp
    , x4lv.num_results
    , coalesce(x4lv.category, m.subcategory_name_en) AS category
    , coalesce(x4lv.parent_category, m.maincategory_name_en) AS parent_category
    , coalesce(x4lv.country_iso2, m.country_iso2)  AS country_iso2
    , coalesce(x4lv.district_name, m.district_name) AS district
    , x4lv.merchant_id AS merchants_array

    from {{ ref('__web_events_1_users') }} e
    --from qdata.__web_events_1_users e
    --
    -- X EVENTS
    LEFT JOIN consumer_web.x_reservation_completed x1rc on x1rc.id=e.id

    LEFT JOIN consumer_web.x_reservation_started x2rs on x2rs.id=e.id
    LEFT JOIN consumer_web.x_details_viewed x3dv on x3dv.id=e.id
    LEFT JOIN (
                  SELECT
                      l.id,
                      l.timestamp,
                      l.sent_at,
                      l.num_results,
                      l.with_availability,
                      l.query,
                      l.experiment_id_variation
                      --
                      -- DESTINATION
                      ,
                      co.country_iso2,
                      CASE
                      -- There is a bug in dest. table with dest_level=3
                      WHEN d.destination_id = 501
                          then 'Wien'
                      ELSE d.city_name
                      END                as city,
                      d.district_name,
                      l.destination_id
                      --
                      -- CUISINES
                      ,
                      l.category_id -- Listed cat ID
                      -- , l.category_name -- Listed Category track is in local, use english to normalize
                      ,
                      c.category_name_en as category,
                      CASE
                      WHEN COALESCE(c.parent_id) in (1, 0)
                          then c.category_id
                      ELSE c.parent_id
                      END                as parent_category_id,
                      CASE
                      WHEN COALESCE(c.parent_id, 0) in (1, 0)
                          then c.category_name_en
                      ELSE c2.category_name_en
                      END                as parent_category,
                      l.merchant_id
                  from consumer_web.x_listing_viewed l
                      left join qdata.destinations d on d.destination_id = l.destination_id
                      left join static.categories_en c on c.category_id = l.category_id
                      left join static.categories_en c2 on c2.category_id = c.parent_id
                      left join qdata_static.countries co on co.country_iso3 = l.destination_country
              ) x4lv on x4lv.id=e.id
    LEFT JOIN consumer_web.x_text_searched xxts on xxts.id=e.id
    LEFT JOIN consumer_web.x_filter_searched xxfs on xxfs.id=e.id
    LEFT JOIN consumer_web.x_customer_logged_in xxcli on xxcli.id=e.id
    LEFT JOIN consumer_web.x_customer_registered xxcr on xxcr.id=e.id
    --
    LEFT JOIN qdata.merchants m on m.merchant_id = coalesce(x1rc.merchant_id, x2rs.merchant_id, x3dv.merchant_id, xxts.merchant_id)
{% if adapter.already_exists(this.schema, this.table) and not flags.FULL_REFRESH %}
where e.timestamp > ( select max(t.timestamp ) from {{this}} t )
    and e.timestamp < ( select max(t.timestamp ) + interval '100 DAYS' from {{this}} t)
{% else %}
where e.timestamp < ( select  min(t.timestamp ) + interval '100 DAYS' from {{ ref('__web_events_1_users') }} t)
{% endif %}
